# Cheatsheets

Sometimes it is really helpful to have a quick glance at features and syntax.

A few general cheat sheets created by RStudio and the community can be found in this folder. Up-to-date version of them and more cheat sheets can be found [here](https://www.rstudio.com/resources/cheatsheets/).
