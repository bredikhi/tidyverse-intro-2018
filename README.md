# Intro to R and tidyverse

Main links: [R language](https://www.r-project.org/about.html) | [CRAN](https://cran.r-project.org/) | [RStudio](https://www.rstudio.com/) and [cheat sheets](https://www.rstudio.com/resources/cheatsheets/) | [Bioconductor](https://www.bioconductor.org/install/)

## References

### R

1. [Evaluating the Design of the R Language, Morandat et at., 2012](http://r.cs.purdue.edu/pub/ecoop12.pdf)

### Tidy data

1. [Tidy data paper](https://cran.r-project.org/web/packages/tidyr/vignettes/tidy-data.html)

1. [Concept of tidy data in the R4DS book](http://r4ds.had.co.nz/tidy-data.html)

### Tidyverse

1. [R for Data Science](https://www.tidyverse.org/learn/)

1. [Workshop by Hadley Wickham](https://github.com/hadley/data-science-in-tidyverse) adapted from RStudio's [Master the Tidyverse](https://github.com/rstudio/master-the-tidyverse)



