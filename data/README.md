Example data for this course is exported from the [nycflights13](https://cran.r-project.org/web/packages/nycflights13/index.html) package for education purposes only.

You can also load this data into R by installing the package:

```
install.packages("nycflights13")
library(nycflights13)
#
# Multiple datasets are available:
# airplanes, airports, flights, planes, weather
#
# E.g.:
weather
```
